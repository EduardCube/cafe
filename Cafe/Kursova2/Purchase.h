#pragma once
#include "Cafe.h"
#include<iostream>
#include<string>
#include<fstream>
#include<Windows.h>
#include<stdlib.h>
#include<vector>
class Purchase : public Cafe
{
protected:
	int* sum = new int;
public:
	Purchase();
	int Order();
	virtual void SetInfo();
	virtual void GetInfo();
	virtual void Addperson();
	virtual void Subperson();
	virtual ~Purchase();
};

